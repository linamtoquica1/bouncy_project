from src.controller import BouncyController


class ProportionController:
    def __init__(self, percentage: int):
        """
        This module allows to find the bouncy number according to percentage given
        Args:
            percentage (int): Value given from user to find bouncy number.
        """
        self.percentage = percentage

    def find_number_according_percentage_proportion(self):
        """
        this method find the bouncy number according to percentage of proportion given.
        Returns:
            bouncy number found (int)

        """
        iterator_number = 99
        bouncy_number = 0

        proportion = self.percentage/100

        while bouncy_number < proportion * iterator_number:
            iterator_number = iterator_number + 1
            bouncy_controller = BouncyController(str(iterator_number))
            if bouncy_controller.is_bouncy_number():
                bouncy_number = bouncy_number + 1

        # print(f"result is: {iterator_number}")
        return iterator_number
