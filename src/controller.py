

class BouncyController:

    def __init__(self, number: str):
        """
        BounceController is a module that allows calculate if a number is increasing, decreasing or bouncy.
        Args:
            number (str): the number which we want to validate.
        """
        self.number = number

    def is_increase_number(self):
        """
        Method to return if a number is an increasing number, for example 134468.

        Returns:
            True if number is an increasing number.
            False in otherwise.

        """

        for i, value in enumerate(self.number):
            if i + 1 < len(self.number) and value > self.number[i + 1]:
                # print(f"{self.number} is not incrementing number, the value {value} is greater "
                #       f"than {self.number[i + 1]}")
                return False
        return True

    def is_decrease_number(self):
        """
        Method to return if a number is an decreasing number, for example 66420.

        Returns:
           True if number is an decreasing number.
           False in otherwise.

        """

        for i, value in enumerate(self.number):
            if i + 1 < len(self.number) and value < self.number[i + 1]:
                # print(f"{self.number} is not a decreasing number, the value {value} is lower than {self.number[i + 1]}")
                return False
        return True

    def is_bouncy_number(self):
        """
        Method to return if a number is an bouncy number, for example 6642350.

        Returns:
          True if number is an bouncy number.
          False in otherwise.

        """

        if self.is_decrease_number() is False and self.is_increase_number() is False and int(self.number) > 100:
            return True
        else:
            return False
