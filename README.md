# Bouncy project

This project is made to resolve the trouble about bouncy numbers.

The following facts are the goals of this project:

- Working from left-to-right if no digit is exceeded by the digit to its left it is called an increasing number; for example, 134468.

- Similarly if no digit is exceeded by the digit to its right it is called a decreasing number; for example, 66420.

- We shall call a positive integer that is neither increasing nor decreasing a "bouncy" number; for example, 155349.

- Clearly there cannot be any bouncy numbers below one-hundred, but just over half of the numbers below one-thousand (525) are bouncy. In fact, the least number for which the proportion of bouncy numbers first reaches 50% is 538.

- Surprisingly, bouncy numbers become more and more common and by the time we reach 21780 the proportion of bouncy numbers is equal to 90%.

- Find the least number for which the proportion of bouncy numbers is exactly 99%.

**Installation guide**

1. Clone the repo: `git clone git@bitbucket.org:linamtoquica1/bouncy_project.git`

2. I recommend use virtualenv for the dependencies management, you has two options: 

- virtualenv: `virtualenv bouncy -p $(which python3)`  
- virtualenvwrapper: `mkvirtualenv bouncy -p $(which python3) -a` .

3. Install dev requirements: `pip install -r requirements_dev.txt`

4. Run project to find solution `python3 main.py` :
There, the console will ask you a percentage and after it will calculate the bouncy number according to the percentage of proportion. 

5. If you want to run test and verify coverage execute: `pytest --cov=src/ --cov-report term-missing ` (Remember you have to work inside a venv)
