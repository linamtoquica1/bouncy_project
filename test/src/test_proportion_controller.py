import pytest

from src.proportion_controller import ProportionController


test_list = [
    (50, 538),
    (90, 21780),
    # (99, 1587000)
]


@pytest.mark.parametrize("input_number, expected_response", test_list)
def test_find_proportion_according_to_percentage(input_number, expected_response):
    controller = ProportionController(input_number)
    assert controller.find_number_according_percentage_proportion() == expected_response
