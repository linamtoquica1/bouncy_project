import pytest

from src.controller import BouncyController


test_increasing_expected = [
    ("12325778", False),
    ("12345778", True)
]


@pytest.mark.parametrize("input_number, expected_response", test_increasing_expected)
def test_find_is_increase_number(input_number, expected_response):
    bouncy_controller = BouncyController(input_number)
    response = bouncy_controller.is_increase_number()
    assert response is expected_response


test_decreasing_expected = [
    ("21234543", False),
    ("87654321", True)
]


@pytest.mark.parametrize("input_number, expected_response", test_decreasing_expected)
def test_find_is_decreasing_number(input_number, expected_response):

    bouncy_controller = BouncyController(input_number)
    response = bouncy_controller.is_decrease_number()
    assert response is expected_response


test_bouncy_expected = [
    ("21", False),
    ("1234", False),
    ("8762345", True)
]


@pytest.mark.parametrize("input_fun, expected", test_bouncy_expected)
def test_find_is_bouncy_number(input_fun, expected):

    bouncy_controller = BouncyController(input_fun)
    response = bouncy_controller.is_bouncy_number()
    assert response is expected
