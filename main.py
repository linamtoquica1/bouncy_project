from src.proportion_controller import ProportionController

if __name__ == '__main__':
    while True:
        print("Please enter the percentage of proportion that you want to find bouncy number:")
        try:
            input_percentage = int(input("percentage (%): "))
        except Exception as e:
            print(e)
        else:
            proportion_controller = ProportionController(input_percentage)
            print(f"The bouncy number found to {input_percentage} is "
                  f"{proportion_controller.find_number_according_percentage_proportion()}")
